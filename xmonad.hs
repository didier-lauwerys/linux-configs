import XMonad
import XMonad.Core
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops
import XMonad.Layout.Grid
import XMonad.Layout.Spacing
import XMonad.Util.EZConfig
import XMonad.Util.Run
import XMonad.Util.EZConfig

myLayouts = avoidStruts $ spacing 4 $
            layoutTall ||| layoutGrid ||| layoutFull
    where
        layoutTall = Tall 1 (3/100) (1/2)
        layoutGrid = Grid
        layoutFull = Full

myWorkspaces = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

myKeys = 
 [ ((mod1Mask, xK_f), spawn "firefox")
 , ((mod1Mask .|. shiftMask, xK_s), spawn "sudo systemctl restart display-manager")
 ]

{-
mySB = statusBarProp "xmobar" (pure xmobarPP)

myScratchpads :: [NamedScratchpad]
myScratchpads = [
    NS "notes" "nvim ~/.notes" defaultFloating
    ] where role = stringProperty "WM_WINDOW_ROLE"

myKeys = 
    [ ((modm .|. shiftMask, xK_a), namedScratchpadAction myScratchpads "notes")
    ]
    -}

main :: IO ()
main = do
    xmobar0 <- spawnPipe ("/usr/bin/xmobar -x 0 $HOME/.xmobarrc")
    xmobar1 <- spawnPipe ("/usr/bin/xmobar -x 1 $HOME/.xmobarrc")
    xmobar2 <- spawnPipe ("/usr/bin/xmobar -x 2 $HOME/.xmobarrc")
    xmonad $ docks $ ewmh def
        { terminal = "alacritty"
        , borderWidth = 2
        , layoutHook = myLayouts
        , normalBorderColor = "#E2F1FF"
        , focusedBorderColor = "#1C1ED4"
        , workspaces = myWorkspaces
        , manageHook = manageDocks
        , logHook = dynamicLogWithPP $ xmobarPP
            { ppOutput = \x -> hPutStrLn xmobar0 x   -- xmobar on monitor 1
            >> hPutStrLn xmobar1 x   -- xmobar on monitor 2
            >> hPutStrLn xmobar2 x   -- xmobar on monitor 2
            -- Current workspace
            }

        } `additionalKeys` myKeys
