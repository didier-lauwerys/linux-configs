" Disable mouse options
set mouse=

" Map jj to escape
imap	jj	<ESC>

" Indentation settings
set autoindent
set smartindent
set expandtab
set shiftwidth=4
set signcolumn
" set colorcolumn=100

syntax off

"Open file under cursor in split view
nnoremap <leader>o yiWq:ivsp <esc>p<cr>

"Keep cursor centered when searching
nnoremap n nzzzv
nnoremap N Nzzzv
nnoremap J mzJ'z

"Undo breakpoints
inoremap , ,<c-g>u
inoremap . .<c-g>u
inoremap ! !<c-g>u
inoremap ? ?<c-g>u

" Make ',' leader
vmap 	,	<leader>
nmap	,	<leader>

vnoremap <leader># :norm 0i#<cr>
vnoremap <leader>!# :norm ^x<cr>

" Set clipboard
set clipboard+=unnamedplus

" " Copy to Clipboard
vnoremap <leader>y "*y
nnoremap <leader>y "*y
nnoremap <leader>Y "*yg_
nnoremap <leader>yy "*yy

" " Paste from Clipboard
nnoremap <leader>p "*p
nnoremap <leader>P "*P
vnoremap <leader>p "*p
vnoremap <leader>P "*P

" To map <Esc> to exit terminal-mode: >
tnoremap <Esc> <C-\><C-n>

" To simulate |i_CTRL-R| in terminal-mode: >
tnoremap <expr> <C-R> '<C-\><C-N>"'.nr2char(getchar()).'pi'

" To use `ALT+{h,j,k,l}` to navigate windows from any mode: >
tnoremap <C-w>h <C-\><C-N><C-w>h
tnoremap <C-w>j <C-\><C-N><C-w>j
tnoremap <C-w>k <C-\><C-N><C-w>k
tnoremap <C-w>l <C-\><C-N><C-w>l
tnoremap <C-w>w <C-\><C-N><C-w>w

" Open config file quickly
nnoremap <leader>H :vs $HOME/.config/nvim/init.vim<cr>

" Persistent undo
set noswapfile
set nobackup
set undofile
set undodir="$HOME/.config/nvim/undodir"

" Omnicomplete
set tags=$HOME/_ddm/mcu-sdk-2.0/tags
set fdm=manual
"set complete-=i
let g:coq_settings = { 'auto_start' : 'shut-up'}

" Keep cursor 8 from bottom and top of screen
set scrolloff=8

" Display line number with relative numbers
set number
set relativenumber

" Disable redrawing screen when running macro or script (better performance)
set lazyredraw

" See title at top of file
set title

" Enable folding
set foldmethod=syntax
set nofoldenable

" Set syntax of .amc files to python
autocmd BufNewFile,BufRead *.amc set syntax=python
" Set color scheme in when using vimdiff (default is terrible)
if &diff
    :colo desert
    :syntax off
endif

" in vimdiff put or get single line
if &diff
    nnoremap <leader>dp V:diffput<cr>
    nnoremap <leader>dg V:diffget<cr>
endif

""" Custom Functions
" Create/open memo file
nnoremap <leader>K :call Customdiff()<cr>
function Customdiff()
  :Gedit a3eceb521eb11db4fddd4759491b9445546a0bbc:%<C-n>
  :Gvdiffsplit 5eef102e8a2598bdf3f98ddb76c44f7edb51ff2b<C-n>
endfunction

" Copy the ticket number to the git message
nnoremap <leader>C :call CopyTicketNum()<cr>
function CopyTicketNum()
  :g/NXH2004-[0-9]\+/y
  :0
  :norm Pd3w2f-D
endfunction

" Create/open memo file
nnoremap <leader>M :call OpenMemo()<cr>
function OpenMemo()
  let b:folder="$HOME/memo"
  let b:filename=expand("%:p")
  let memoname=substitute(b:filename, '\.', '_', "g")
  let memoname=substitute(memoname, '/', '.', "g")
  execute 'split ' . b:folder . '/' . memoname
endfunction

call plug#begin('~/.vim/plugged')
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-telescope/telescope.nvim'
Plug 'ThePrimeagen/vim-be-good'
Plug 'nvim-tree/nvim-web-devicons'
" main one
Plug 'ms-jpq/coq_nvim', {'branch': 'coq'}
" 9000+ Snippets
Plug 'ms-jpq/coq.artifacts', {'branch': 'artifacts'}

" lua & third party sources -- See https://github.com/ms-jpq/coq.thirdparty
" Need to **configure separately**

Plug 'ms-jpq/coq.thirdparty', {'branch': '3p'}
" - shell repl
" - nvim lua api
" - scientific calculator
" - comment banner
" - etc
Plug 'willchao612/vim-diagon', {'branch': 'main'}
Plug 'vim-scripts/DrawIt', {'branch': 'master'}
Plug 'bluz71/vim-nightfly-colors', { 'as': 'nightfly' }
Plug 'Th3Whit3Wolf/space-nvim'
Plug 'ellisonLeao/gruvbox.nvim'
Plug 'EdenEast/nightfox.nvim'
Plug 'nekonako/xresources-nvim'
call plug#end()

set background=dark
colorscheme gruvbox

" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fs <cmd>Telescope grep_string<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>
nnoremap <leader>ft <cmd>Telescope treesitter<cr>

vnoremap <leader>dS :Diagon Sequence<cr>
vnoremap <leader>dGP :Diagon GraphPlanar<cr>
vnoremap <leader>dGD :Diagon GraphDAG<cr>

vnoremap <leader>dF :Diagon Flowchart<cr>
