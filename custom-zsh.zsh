alias v=nvim
alias gstat="vim -c 'Git | wincmd j | hide'"

# Git worktree alias
alias wt="git worktree"
alias wtl="git worktree list"
alias wta="git worktree add"
alias wtr="git worktree remove"


plugins=(git vi-mode)

VI_MODE_SET_CURSOR=true
